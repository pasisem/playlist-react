import React, { useState } from "react";
import "./Register.css";

export default function Register() {
  const [form, setForm] = useState({
    username: "",
    password: "",
    password2: "",
    name: "",
    email: "",
    bio: "",
  });

  const [valid, setValid] = useState({
    username: true,
    password: true,
    password2: true,
    name: true,
    email: true,
    bio: true,
  });

  const handleOnChange = (event) => {
    const { name, value } = event.target;
    setForm({ ...form, [name]: value });
    setValid({ ...valid, [name]: true });
  };

  const checkValidation = () => {
    let usernameVal, passwordVal, password2Val, emailVal;

    if (form.username.length > 3) usernameVal = true;
    else usernameVal = false;

    if (form.password.length > 6) passwordVal = true;
    else passwordVal = false;

    if (form.password2.length > 6 && form.password === form.password2)
      password2Val = true;
    else password2Val = false;

    if (
      form.email.length > 3 &&
      form.email.includes("@") &&
      form.email.includes(".")
    )
      emailVal = true;
    else emailVal = false;

    if (usernameVal && passwordVal && password2Val && emailVal) return true;
    else {
      setValid({
        username: usernameVal,
        password: passwordVal,
        password2: password2Val,
        name: true,
        email: emailVal,
        bio: true,
      });
      return false;
    }
  };

  const submitForm = (e) => {
    e.preventDefault();

    if (checkValidation()) {
      console.log(form);
      alert(form);
    }
  };

  const cancelForm = (e) => {
    e.preventDefault();

    setForm({
      username: "",
      password: "",
      password2: "",
      name: "",
      email: "",
      bio: "",
    });

    setValid({
      username: true,
      password: true,
      password2: true,
      name: true,
      email: true,
      bio: true,
      all: false,
    });

    alert("form go bye bye.");
  };

  return (
    <div className="register">
      <h1>register</h1>
      <br />
      <div>
        <form className="form">
          <div
            className="labelInputContainer"
            title="username unique to you. letters and numbers only."
          >
            <label>username</label>
            <input
              type="text"
              className={valid.username ? "" : "notValid"}
              value={form.username}
              name="username"
              onChange={(e) => handleOnChange(e)}
            />
          </div>

          <div
            className="labelInputContainer"
            title="a password. can't be too short."
          >
            <label>password</label>
            <input
              type="password"
              className={valid.password ? "" : "notValid"}
              value={form.password}
              name="password"
              onChange={(e) => handleOnChange(e)}
            />
          </div>

          <div className="labelInputContainer" title="your password, again.">
            <label>password 2</label>
            <input
              type="password"
              className={valid.password2 ? "" : "notValid"}
              value={form.password2}
              name="password2"
              onChange={(e) => handleOnChange(e)}
            />
          </div>

          <div
            className="labelInputContainer"
            title="name of your choosing. any characters."
          >
            <label>name</label>
            <input
              type="text"
              className={valid.name ? "" : "notValid"}
              value={form.name}
              name="name"
              onChange={(e) => handleOnChange(e)}
            />
          </div>

          <div className="labelInputContainer" title="your email.">
            <label>email</label>
            <input
              type="text"
              className={valid.email ? "" : "notValid"}
              value={form.email}
              name="email"
              onChange={(e) => handleOnChange(e)}
            />
          </div>

          <div className="labelInputContainer" title="something about you.">
            <label>bio</label>
            <textarea
              className={valid.bio ? "" : "notValid"}
              value={form.bio}
              name="bio"
              onChange={(e) => handleOnChange(e)}
            />
          </div>

          <div
            className="labelInputContainer"
            title="submit the form when ready. or cancel."
          >
            <label>done?</label>
            <input
              className={"buttonSubmit"}
              type="submit"
              value="submit"
              onClick={submitForm}
            />
            <input
              className={"buttonCancel"}
              type="button"
              value="cancel"
              onClick={cancelForm}
            />
          </div>
        </form>
      </div>
    </div>
  );
}
