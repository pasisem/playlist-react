import React from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import Register from './views/Register';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Header />
      </header>
      <div className="App-page">
        <Register />
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}

export default App;
